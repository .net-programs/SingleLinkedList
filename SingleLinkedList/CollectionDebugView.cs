﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SingleLinkedList
{
    /// <summary>
    /// Прокси-тип коллекции.
    /// </summary>
    internal class CollectionDebugView<T>
    {
        private ICollection<T> collection;

        /// <summary>
        /// Создаёт прокси-тип для указанной коллекции.
        /// </summary>
        /// <param name="collection">Коллекция.</param>
        public CollectionDebugView(ICollection<T> collection)
        {
            this.collection = collection ?? throw new ArgumentNullException(nameof(collection));
        }

        /// <summary>
        /// Элементы коллекции.
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public T[] Items
        {
            get
            {
                T[] items = new T[collection.Count];
                collection.CopyTo(items, 0);
                return items;
            }
        }
    }
}